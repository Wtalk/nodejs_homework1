const express = require('express');
const cors = require('cors')
const app = express();
const port = 8080;
const morgan = require('morgan')

const {saveFile, getListOfFiles, getFile} = require('./utils');

app.use(express.json())
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))
app.use(cors())

app.post('/api/files', (req, res) => {
    const {filename, content} = req.body;

    const [message, code] = saveFile(filename, content);

    res.status(code);
    res.send({message})
})

app.get('/api/files', (req, res) => {
    const [message, files] = getListOfFiles(res.statusCode);

    res.status(res.statusCode)
    res.send({
        message,
        files
    })
})

app.get('/api/files/:filename', (req, res) => {
    const [data, code] = getFile(req.params.filename);
    res.status(code);
    res.send(data);
})

app.listen(port, (error) => {
    error ? console.log(error) : console.log(`Server is working on ${port} port`)
})