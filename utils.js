const fs = require('fs');

const saveFile = (filename, content) => {
    if (!content) {
        return ["Please specify 'content' parameter", 400];
    } else if (!filename) {
        return ["Please specify 'filename' parameter", 400];
    } else if (fs.existsSync('./files/' + filename)) {
        return ["Such file already exists!", 400];
    } else if (filename && content) {
        fs.writeFileSync('./files/' + filename, content);
        return ["File created successfully", 200];
    } else {
        return ["Server error", 500]
    }
}

const getListOfFiles = (statusCode) => {
    if (statusCode === 400) {
        return ['Client error']
    } else if (statusCode === 200) {
        const files = fs.readdirSync('./files/').map(file => file)
        return ['Success', files]
    } else {
        return ['Server error']
    }
}


const getFile = filename => {
    const isExist = fs.existsSync('./files/' + filename)
    if (!isExist) {
        return [
            {
                message: `No file with ${filename} filename found`
            }
            , 400];
    } else if (isExist) {
        const content = fs.readFileSync('./files/' + filename).toString();
        const extension = filename.split('.')[1];
        const uploadedDate = fs.statSync('./files/' + filename).mtime
        return [{
            message: "Success",
            filename,
            content,
            extension,
            uploadedDate
        }, 200]
    } else {
        return [
            {"message": "Server error"}, 500
        ]
    }
}

module.exports = {
    saveFile,
    getListOfFiles,
    getFile,
};